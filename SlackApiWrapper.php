<?php

use GuzzleHttp\Client;

class SlackApiWrapper
{
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public static function send($text, $username = '커스텀 알림', $icon_emoji = ':newspaper:')
    {
        $endpoint = $_ENV['SLACK_WEBHOOK_URL'];
        if (!$endpoint) {
            throw new \Exception('LOG_SLACK_WEBHOOK_URL 환경 변수가 없어서 슬랙 커스텀 알림을 보내지 못했습니다.');
        }
        $client = new Client();
        $client->request('POST', $endpoint, [
            'json' => [
                'text' => $text,
                'username' => $username,
                'icon_emoji' => $icon_emoji
            ],
        ]);
    }
}
