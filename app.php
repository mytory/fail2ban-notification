<?php
require 'bootstrap.php';
require 'SlackApiWrapper.php';

$before = (string) time() - (60 * 60 * $_ENV['BEFORE_HOURS']);
$sql = "select jail, ip, timeofban, bantime from bips where timeofban >= ? group by ip, jail";
try {
    $prepared = $pdo->prepare($sql);
} catch (PDOException $e) {
    $sql = "select jail, ip, timeofban from bans where timeofban >= ? group by ip, jail";
    $prepared = $pdo->prepare($sql);
}

$prepared->execute([$before]);
$rows = $prepared->fetchAll();
$count = count($rows);

if ($count === 0) {
    $message = "지난 {$_ENV['BEFORE_HOURS']}시간 동안 차단한 IP가 없습니다.";
    if (!empty($_ENV['ENV']) and $_ENV['ENV'] === 'local') {
        SlackApiWrapper::send($message, $_ENV['SLACK_USERNAME'], $_ENV['SLACK_ICON_EMOJI']);
    } else {
        echo $message . PHP_EOL;
    }
}

$result = [];

foreach ($rows as $row) {
    $flag = trim(strtolower(file_get_contents("http://ipinfo.io/{$row->ip}/country")));
    $result[$row->jail][] =  ":flag-{$flag}:" .  $row->ip;
}

$message_array = [
    "지난 {$_ENV['BEFORE_HOURS']}시간 동안 차단한 IP 목록({$count}개):",
];
foreach ($result as $jail => $ips) {
    $message_array[] = "- $jail: " . implode(', ', $ips);
}

$message = implode("\n", $message_array);

if (!empty($_ENV['ENV']) and $_ENV['ENV'] === 'local') {
    echo $message . PHP_EOL;
} else {
    SlackApiWrapper::send($message, $_ENV['SLACK_USERNAME'], $_ENV['SLACK_ICON_EMOJI']);
}
