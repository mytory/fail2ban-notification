# 정한 시간에 따라 Fail2ban이 차단한 IP 목록을 Slack으로 받기

![](screenshot.png)

## 기능

지정한 시간(예컨대 24시간)동안 Fail2ban이 차단한 IP를, 어떤 규칙에 따라 차단했는지, 차단한 IP는 어떤 국가의 IP인지 확인해서 슬랙 알림으로 보내 줍니다.

- 시간을 지정할 수 있습니다.
- 차단한 규칙에 따라 분류해서 보여 줍니다.
- IP 앞에 국기를 넣어 줍니다.
- 슬랙 알림을 보낼 때 이름과 이모지를 커스터마이징할 수 있습니다.

아래 안내대로 설치/설정한 뒤 Cron에 등록하시면 됩니다.

(fail2ban 0.10과 0.11에서 테스트했습니다.)

## 프로그램 복사

원하는 경로에 가서 git을 clone합니다.

```bash
cd /path/to/install
git clone https://gitlab.com/mytory/fail2ban-notification.git
cd fail2ban-notification
composer install

# 마지막으로 환경설정 파일을 복사합니다.
cp .env.example .env
```

## PHP sqlite 확장 설치

우분투

```bash
apt install php-sqlite3
```


## 슬랙 웹훅 URL 만들기

슬랙 가입과 사용법은 생략하겠습니다. 잘 모르시면 그것부터 알아 오셔야 합니다.

슬랙 웹훅 URL을 받는 방법은 GitLab의 설명을 복사했습니다.

1. 슬랙 팀에 로그인하고 [새 인커밍 웹훅 설정을 시작](https://my.slack.com/services/new/incoming-webhook)한다.
2. 기본 알림을 받을 슬랙 채널을 선택한다. 설정을 추가하기 위해 **Add Incoming WebHooks integration**을 누른다.
3. Webhook URL을 복사한다.


## fail2ban DB 파일 위치 확인

아래 명령어로 확인합니다.

```bash
sudo fail2ban-client get dbfile
```

결과값은 아래와 같은 식입니다.

```
Current database file is:
`- /var/lib/fail2ban/fail2ban.sqlite3
```

## 설정

이제 `.env` 파일을 채웁시다.

```env
# 실제 서버 환경이라면 production, 로컬 테스트중이라면 local이라고 적습니다.
ENV=production

# fail2ban은 기본적으로 sqlite를 사용합니다.
DB_CONNECTION=sqlite

# 앞서 확인했던 fail2ban.sqlite3 파일 경로
DB_DATABASE=/var/lib/fail2ban/fail2ban.sqlite3

# 몇 시간동안 차단한 ip 목록을 가져올 건지. 기본값은 24시간.
BEFORE_HOURS=24

# 앞에서 복사해 둔 SLACK 웹훅 URL
SLACK_WEBHOOK_URL=https://hooks.slack.com/services/xxxxx

# 슬랙 알림 보낼 이름
SLACK_USERNAME='Fail2ban'

# 슬랙 알림 보낼 때 이모지. 슬랙에서 사용하는 이름을 사용하면 된다.
SLACK_ICON_EMOJI=':newspaper:'
```

## cron 등록

우선 `which php` 명령으로 글로벌 명령어 `php` 파일 경로를 확인합니다. 저는 `/usr/bin/php`라고 나오네요. 실행할 파일명은 `app.php`입니다.

그러면 명령은 아래와 같겠죠. (`/path/to/install`은 당연히 실제 경로로 변경해 주셔야 합니다.)

```bash
/usr/bin/php /path/to/install/fail2ban-notification/app.php
```

root 권한으로 위 명령을 실행해서 에러가 없는지 확인해 봅니다.

슬랙 알림이 오면 성공입니다. 에러가 나면 에러 메시지에 따라 처리합니다.

문제가 없다면 좋아하는 에디터로 `/etc/crontab`을 엽니다. nano나 vi를 사용하면 되겠죠.

cron 규칙에 따라 맨 밑에 아래 줄을 추가합니다. 아래 줄은 매일 아침 10시에 슬랙 알림을 보내 주는 것입니다.

```
0 10    * * *   root    /usr/bin/php /path/to/install/fail2ban-notification/app.php > /dev/null 2>&1
```

` > /dev/null 2>&1`는 에러 메시지가 나오지 않게 처리하는 것입니다. cron은 에러 메시지가 나오면 로컬의 mail에 누적하거든요.


## 여러 서버에서 프로그램을 한꺼번에 업데이트하기

이 프로그램이 앞으로 업데이트가 될지는 잘 모르겠습니다. 하지만 그런 일이 있다면 여러 서버에 프로그램을 설치한 경우 하나씩 들어가서 업데이트하는 것이 번거로울 것입니다.

이를 위해 쉘 스크립트 예제를 넣어 뒀습니다. 아래 명령어로 쉘 스크립트를 만듭니다.

``` bash
cp remote-git-pull.sh.example remote-git-pull.sh

# 실행 권한 부여
chmod +x remote-git-pull.sh
```

파일을 열면 모양이 아래와 같습니다.

``` bash
#!/usr/bin/env bash
ssh server1@domain.com 'cd /path/to/install/fail2ban-notification && git pull'
ssh server2@domain.com 'cd /path/to/install/fail2ban-notification && git pull'
ssh server3@domain.com 'cd /path/to/install/fail2ban-notification && git pull'
ssh server4@domain.com 'cd /path/to/install/fail2ban-notification && git pull'
ssh server5@domain.com 'cd /path/to/install/fail2ban-notification && git pull'
```

`server1@domain.com`에 적당한 목적지를, `cd /path/to/install/fail2ban-notification` 대신 설치 경로를 넣어 주시면 됩니다.

이렇게 만든 뒤 실행은 `./remote-git-pull.sh`로 하면 되겠죠.


## 나가며

원래 [Fail2ban action](https://agvber.tistory.com/8)을 만들어서 IP를 차단할 때마다 슬랙 알림을 받았는데, 이게 너무 많이 오더군요. 그래서 만들게 됐습니다.